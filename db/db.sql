CREATE TABLE product (
    id INT PRIMARY KEY auto_increment,
    title VARCHAR(20),
    description VARCHAR(200),
    price FLOAT
);
