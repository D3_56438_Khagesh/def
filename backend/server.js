const express = require('express')
const cors = require('cors')

const app = express()

app.use(express.json())
app.use(cors('*'))
const routerProduct = require('./routes/product')
app.use('/product', routerProduct)

app.listen(4001, ()=>{
    console.log('Server started on port 4000')
})