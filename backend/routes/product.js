const express = require('express')

const db = require('../db')

const router = express.Router()

router.post('/add', (request, response)=>{
    const{title, price, description} = request.body
    const connection = db.openConnection()

    const statement = `
    INSERT INTO product
        (title, price, description)
    VALUES
        ('${title}', '${price}', '${description}')
    `
    connection.query(statement, (error, result)=>{
        connection.end();
        if(error){
            response.send(error)
        }
        else{
            response.send(result)
        }
    })
})

router.get('/getall', (request, response)=>{
    
    const connection = db.openConnection()

    const statement = `
    SELECT (title, price, description)  FROM product
    `
    connection.query(statement, (error, result)=>{
        connection.end();
        if(error){
            response.send(error)
        }
        else{
            response.send(result)
        }
    })
})

module.exports = router;